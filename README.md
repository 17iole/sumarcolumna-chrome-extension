# README #

Chrome plugin to calculate sums in a table row.

### For now ###
* it always looks for a table with id="tabla"
* it sums the 3rd column
* it throws error if not found

### In future it will allow you to ###

* choose table id
* choose column number

### In far away future... ###

* select table by clicking on it
* inject sum into the DOM of the webpage
* ...what more?
